#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/spinlock.h>
#include <linux/sched.h>
#include <linux/types.h>
#include <linux/fcntl.h>
#include <linux/hdreg.h>
#include <linux/genhd.h>
#include <linux/blkdev.h>

#define MAXBUF 1024
#define BLK_MAJOR 250
#define BLK_BYTES 1024*1024*64
char blk_dev_name[]="myblkdev";
static char flash[BLK_BYTES];

int major;
spinlock_t lock;
struct gendisk *gd;

/*块设备数据传输*/
static void blk_transfer(unsigned long sector, unsigned long nsect, char *buffer, int write)
{
	int read = !write;
	if(read){
		memcpy(buffer, flash+sector*512, nsect*512);
	}else{
		memcpy(flash+sector*512, buffer, nsect*512);
	}
}

/*块设备请求处理函数*/
static void blk_request_func(struct request_queue *q)
{
	struct request *req;
	req = blk_fetch_request(q);
	while(req != NULL){
		unsigned long start = blk_rq_pos(req) << 9;
		unsigned long len = blk_rq_cur_sectors(req) << 9;
		
		if(!blk_fs_request(req)){
			__blk_end_request_cur(req,-EIO);
			req = blk_fetch_request(q);
			continue;
		}
		
		if(start + len > BLK_BYTES){
			__blk_end_request_cur(req,-EIO);
			req = blk_fetch_request(q);
			continue;
		}
		
		blk_transfer(blk_rq_pos(req), blk_rq_cur_sectors(req), req->buffer, rq_data_dir(req));
		if(!__blk_end_request_cur(req, 0)){
			req = blk_fetch_request(q);
		}
	}
}

static int blk_ioctl(struct block_device *dev, fmode_t no, unsigned cmd, unsigned long arg)
{
	printk("#############\n");
	return -ENOTTY;
}

static int blk_open(struct block_device *dev, fmode_t no)
{
	printk("blk mount succeed\n");
	return 0;
}

static int blk_release(struct gendisk *gd, fmode_t no)
{
	printk("blk umount succeed\n");
	return 0;
}

struct block_device_operations blk_ops={
	.owner = THIS_MODULE,
	.open = blk_open,
	.release = blk_release,
	.ioctl = blk_ioctl,
};

static int __init myblkdev_init(void)
{
	major = register_blkdev(BLK_MAJOR,blk_dev_name);
	if(major < 0){
		printk("unable to get major number\n");
		return -EBUSY;
	}else if(major == 0){
		major = BLK_MAJOR;
		printk("regiser blk dev succeed\n");
	}
	
	gd = alloc_disk(1);
	if (!gd) {
        printk("alloc_disk failure\n");
		blk_cleanup_queue(gd->queue);
		return -ENOMEM;
    }
	spin_lock_init(&lock);
	gd->major = major;
	gd->first_minor = 0;
	gd->fops = & blk_ops;
	gd->queue = blk_init_queue(blk_request_func,&lock);
	snprintf(gd->disk_name,32,"myblkdev%c",'a');
	blk_queue_logical_block_size(gd->queue,512);
	set_capacity(gd,BLK_BYTES>>9);
	add_disk(gd);
	printk("gendisk init success\n");
	return 0;
}

static void __exit myblkdev_exit(void)
{
	blk_cleanup_queue(gd->queue);
	del_gendisk(gd);
	unregister_blkdev(major,blk_dev_name);
	printk("blk dev exit success!\n");
}

module_init(myblkdev_init);
module_exit(myblkdev_exit);

MODULE_AUTHOR("Fang Xieyun");
MODULE_LICENSE("GPL");
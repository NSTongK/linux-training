#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/skbuff.h>
#include <linux/init.h>
#include <linux/ip.h>
#include <linux/types.h>
#include <linux/sched.h>
#include <linux/netlink.h>
#include <net/sock.h>
#include <net/netlink.h>

#define NETLINK_MYTEST 20
#define MAX_MSGSIZE 1024
struct sock *nl_sk = NULL;

static void sendnlmsg(char *message, int dstpid)
{
	struct sk_buff *skb;
	struct nlmsghdr *nlh;
	int len = NLMSG_SPACE(MAX_MSGSIZE);
	int slen = 0;
	if(!message || !nl_sk){
		return ;
	}
	/*为新的sk_buffer申请空间*/
	skb = alloc_skb(len,GFP_KERNEL);
	if(!skb){
		printk(KERN_ERR "my_net_link:alloc_skb Error!\n");
		return ;
	}
	slen = strlen(message);
	
	/*用nlmsg_put来设置netlink消息头部*/
	nlh = nlmsg_put(skb,0,0,0,MAX_MSGSIZE,0);
	
	/*设置Netlink的控制块*/
	NETLINK_CB(skb).pid = 0;/*消息发送者的id标识，如果是内核发的则置0*/
	NETLINK_CB(skb).dst_group = 0;/*如果目的的组为内核或某一进程，该字段为0*/
	
	message[slen] = '\0';
	memcpy(NLMSG_DATA(nlh),message,slen+1);
	
	/*通过netlink_unicast将消息发送给用户空间*/
	printk("kernel:begin send\n");
	netlink_unicast(nl_sk,skb,dstpid,0);
	printk("kernel:send OK!\n");
	return ;
}

static void nl_data_ready(struct sk_buff* skb)
{
	struct nlmsghdr *nlh;
	u64 rlen;
	void *data;
	
	while(skb->len >= NLMSG_SPACE(0)){
		nlh = nlmsg_hdr(skb);
		printk("nlh->nlmsg_pid=%u skb->len =%d NLMSG_SPACE(0)=%d\n",nlh->nlmsg_pid,skb->len,NLMSG_SPACE(0));
		if(nlh->nlmsg_len < NLMSG_HDRLEN || skb->len < nlh->nlmsg_len)
			return ;
		
		rlen = NLMSG_ALIGN(nlh->nlmsg_len);
		if(rlen > skb->len)
			rlen = skb->len;
		
		data = NLMSG_DATA(nlh);
		printk("received netlink message: %s\n",(char *)data);
		skb_pull(skb, rlen);
		
		sendnlmsg("send from kernel",nlh->nlmsg_pid);
	}
	printk("received finished!\n");
}

static int __init myinit_module(void)
{
	printk("my netlink in!\n");
	nl_sk = netlink_kernel_create(&init_net, NETLINK_MYTEST, 0, nl_data_ready, NULL, THIS_MODULE);
	if(!nl_sk){
        printk(KERN_ERR "my_net_link: create netlink socket error!\n");
        return 1;
    }else{
		return 0;
	}
}

static void __exit myexit_module(void)
{
	printk("my netlink out!\n");
	if(nl_sk != NULL){
		sock_release(nl_sk->sk_socket);
	}
}

MODULE_AUTHOR("Fang Xieyun");
MODULE_LICENSE("GPL");

module_init(myinit_module);
module_exit(myexit_module);

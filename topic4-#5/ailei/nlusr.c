#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include <asm/types.h>
#include <linux/netlink.h>
#include <linux/socket.h>

#define MAX_PAYLOAD 1024 /*消息最大负载为1024字节*/

int main(int argc, char *argv[])
{
	struct sockaddr_nl dest_addr;
    struct nlmsghdr *mynlh = NULL, *mynlh2 = NULL;
    struct iovec iov;
    int sock_fd=-1;
    struct msghdr mymsg;

    if(-1 == (sock_fd=socket(PF_NETLINK, SOCK_RAW,NETLINK_TEST))){
          perror("can't create netlink socket!");
          return 1;
    }

    memset(&dest_addr, 0, sizeof(dest_addr));
    dest_addr.nl_family = AF_NETLINK;
    dest_addr.nl_pid = 0; /*我们的消息是发给内核的*/
    dest_addr.nl_groups = 0; /*在本示例中不存在使用该值的情况*/

    if(-1 == bind(sock_fd, (struct sockaddr*)&dest_addr, sizeof(dest_addr))){
          perror("can't bind sockfd with sockaddr_nl!");
          return 1;

    }

    if(NULL == (mynlh=(struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PAYLOAD)))){
          perror("alloc mem failed!");
          return 1;
    }
    memset(mynlh,0,MAX_PAYLOAD);
    mynlh->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD);
    mynlh->nlmsg_pid = getpid();//我们希望得到内核回应，所以得告诉内核我们ID号
    mynlh->nlmsg_type = NLMSG_NOOP; //指明我们的Netlink是消息负载是一条空消息
    mynlh->nlmsg_flags = 0;

	if(NULL == (mynlh2=(struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PAYLOAD)))){
          perror("alloc mem failed!");
          return 1;
    }
    memset(mynlh2,0,MAX_PAYLOAD);
    mynlh2->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD);
    mynlh2->nlmsg_pid = getpid();//我们希望得到内核回应，所以得告诉内核我们ID号
    mynlh2->nlmsg_type = NLMSG_NOOP; //指明我们的Netlink是消息负载是一条空消息
    mynlh2->nlmsg_flags = 0;
	
    /*设置Netlink的消息内容，来自我们命令行输入的第一个参数*/
    strcpy(NLMSG_DATA(mynlh), argv[1]);

    memset(&iov, 0, sizeof(iov));
    iov.iov_base = (void *)mynlh;
    iov.iov_len = mynlh->nlmsg_len;
    memset(&mymsg, 0, sizeof(mymsg));
    mymsg.msg_iov = &iov;
    mymsg.msg_iovlen = 1;

    sendmsg(sock_fd, &mymsg, 0); //通过Netlink socket向内核发送消息 

    //接收内核消息的消息
    printf("waiting message from kernel!\n");
    memset((char*)NLMSG_DATA(mynlh),0,1024);
    recvmsg(sock_fd,&mymsg,0);
    printf("Got response: %s\n",NLMSG_DATA(mynlh));

	strcpy(NLMSG_DATA(mynlh2), argv[2]);
	sendto(sock_fd,mynlh2,NLMSG_LENGTH(MAX_PAYLOAD),0,(struct sockaddr*)(&dest_addr),sizeof(dest_addr));
	printf("waiting message from kernel!\n");
	memset(mynlh2,0,MAX_PAYLOAD); //清空整个Netlink消息头包括消息头和负载
	recvfrom(sock_fd,mynlh2,NLMSG_LENGTH(MAX_PAYLOAD),0,(struct sockaddr*)(&dest_addr),NULL);
	printf("Got response: %s\n",NLMSG_DATA(mynlh2)); 
	
    close(sock_fd);
    free(mynlh);
	free(mynlh2);
    return 0;
}
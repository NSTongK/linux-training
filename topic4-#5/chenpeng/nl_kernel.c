#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/skbuff.h>
#include <linux/init.h>
#include <linux/ip.h>
#include <linux/types.h>
#include <linux/sched.h>
#include <net/sock.h>
#include <linux/netlink.h>

//#define NETLINK_TEST 20
#define MAX_MSGSIZE 1024

MODULE_AUTHOR("Chen Peng");
MODULE_LICENSE("Dual BSD/GPL");

struct sock *sk = NULL;

static void sendnlmsg(char *message, int dstPID)
{
	struct sk_buff *skb;
	struct nlmsghdr *nlh;
	int len = NLMSG_SPACE(MAX_MSGSIZE);
	int slen = 0;
	skb = alloc_skb(len, GFP_KERNEL);//
	//slen = strlen(message) + 1;
	slen = strlen(message);
	nlh = nlmsg_put(skb, 0, 0, 0, MAX_MSGSIZE, 0);//设置netlink消息头部
	//设置netlink的控制块
	NETLINK_CB(skb).pid = 0;//消息发送者的id标识，内核发的则置为0
	NETLINK_CB(skb).dst_group = 0;//目的组为内核或某一进程，则该字段置为0
	
	message[slen] = '\0';
	memcpy(NLMSG_DATA(nlh), message, slen + 1);
	//通过netlink_unicast()将消息发送用户空间由dstPID所指定了进程号的进程
    netlink_unicast(sk, skb, dstPID, 0);
    printk("send OK!\n");
    return;
}

static void nl_data_ready(struct sk_buff *__skb)
{
	struct sk_buff *skb = skb_get(__skb);
	struct nlmsghdr *nlh = NULL;
	nlh = nlmsg_hdr(skb);//return (struct nlmsghdr *)skb->data
    printk("%s: received netlink message: %s \n", __FUNCTION__, (char*)NLMSG_DATA(nlh));
    kfree_skb(skb);
	sendnlmsg("Oh,I see you", nlh->nlmsg_pid);
	printk("received finished \n");
}

static int __init nl_init(void)
{
	printk("my netlink init\n");
	sk = netlink_kernel_create(&init_net, NETLINK_TEST, 0, nl_data_ready, NULL, THIS_MODULE);
	return 0;
}

static void __exit nl_exit(void)
{
	printk("my netlink exit\n");
	if(sk)
		sock_release(sk->sk_socket);
}

module_init(nl_init);
module_exit(nl_exit);
#include<linux/init.h>
#include<linux/module.h>
#include<linux/types.h>
#include<linux/fs.h>
#include<linux/errno.h>
#include<linux/cdev.h>
#include<linux/kernel.h>
#include<linux/slab.h>

MODULE_LICENSE("GPL");

static int zero_major = 0;
module_param(zero_major, int, 0);

static struct cdev zeroDev;

int zero_open(struct inode *inode, struct file *filp)
{
	return 0;
}

int zero_release(struct inode *inode, struct file *filp)
{
	return 0;
}

static ssize_t zero_read(struct file *filp, char __user *buf, size_t size, loff_t *ppos)
{
	//int ret = 1;
	int i=0;
	for (; i<size; i++) {
		buf[i++] = 0;
	}
	printk("read %d bytes.\n", size);
	return size;
}

static ssize_t zero_write(struct file *filp, const char __user *buf, size_t size, loff_t *ppos)
{
	int ret = -1;

	return ret;
}

static const struct file_operations fops = {
	.owner		= THIS_MODULE,
	.open		= zero_open,
	.release	= zero_release,
	.read		= zero_read,
	.write		= zero_write,
};
	
static int zero_init(void)
{
	int ret, devno;
	dev_t dev = MKDEV(zero_major, 0);

	if (zero_major)
		ret = register_chrdev_region(dev, 1, "zero");
	else {
		ret = alloc_chrdev_region(&dev, 0, 1, "zero");
		zero_major = MAJOR(dev);
	}
	if (ret < 0) {
		printk("zero: unable to get major %d.\n", zero_major);
		return ret;
	}
	
	devno = MKDEV(zero_major, 0);
	cdev_init(&zeroDev, &fops);
	zeroDev.owner = THIS_MODULE;
	zeroDev.ops = &fops;
	ret = cdev_add(&zeroDev, devno, 1);
	if (ret) {
		printk("zero: error %d adding zero.\n", ret);
	}
	
	return 0;
}

static void zero_exit(void)
{
	cdev_del(&zeroDev);
	unregister_chrdev_region(MKDEV(zero_major, 0), 1);
}

module_init(zero_init);
module_exit(zero_exit);
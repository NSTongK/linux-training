#include<linux/init.h>
#include<linux/module.h>
#include<linux/types.h>
#include<linux/fs.h>
#include<linux/errno.h>
#include<linux/cdev.h>
#include<linux/kernel.h>
#include<linux/slab.h>
#include<linux/uaccess.h>

//#include "mycdev.h"

#ifndef MYCDEV_SIZE
#define MYCDEV_SIZE 4094
#endif

MODULE_LICENSE("GPL");

static int mycdev_major = 0;
module_param(mycdev_major, int, 0);

static struct cdev mycDev;

struct mem_dev {
	char *data;
	unsigned long size;
};
struct mem_dev *mem_devp;

int mycdev_open(struct inode *inode, struct file *filp)
{
	//printk("null_open here.\n");
	return 0;
}

int mycdev_release(struct inode *inode, struct file *filp)
{
	//printk("null_release here.\n");
	return 0;
}

static ssize_t mycdev_read(struct file *filp, char __user *buf, size_t size, loff_t *ppos)
{
	int ret = 0;
	unsigned long p = *ppos;
	unsigned long count = size;
	struct mem_dev *dev = mem_devp;
	//printk("null_read here.\n");
	if (p >= MYCDEV_SIZE)
		return 0;
	if (count > MYCDEV_SIZE - p)
		count = MYCDEV_SIZE - p;
	if (copy_to_user(buf, (void*)(dev->data + p), count))
		ret = -EFAULT;
	else {
		*ppos += count;
		ret = count;
		printk(KERN_INFO "read %ld bytes from %ld.\n", count, p);
	}
	
	return ret;
}

static ssize_t mycdev_write(struct file *filp, const char __user *buf, size_t size, loff_t *ppos)
{
	int ret = 0;
	unsigned long p = *ppos;
	unsigned long count = size;
	struct mem_dev *dev = mem_devp;
	//printk("null_write here.\n");
	if (p >= MYCDEV_SIZE)
		return 0;
	if (count > MYCDEV_SIZE - p)
		count = MYCDEV_SIZE - p;
	if (copy_from_user(dev->data + p, buf, count))
		ret = -EFAULT;
	else {
		*ppos += count;
		ret = count;
		printk(KERN_INFO "write %ld bytes to %ld.\n", count ,p);
	}
	printk("write %d bytes.\n", size);
	return ret;
}

static loff_t mycdev_llseek(struct file *filp, loff_t offset, int whence)
{
	loff_t newpos;
	switch (whence) {
		case 0:
			newpos = offset;
			break;
		case 1:
			newpos = filp->f_pos + offset;
			break;
		case 2:
			newpos = MYCDEV_SIZE - 1 + offset;
			break;
		default:
			return -EINVAL;
	}
	if ((newpos<0) || (newpos>MYCDEV_SIZE))
		return -EINVAL;
	
	filp->f_pos = newpos;
	return newpos;
}

static const struct file_operations fops = {
	.owner		= THIS_MODULE,
	.open		= mycdev_open,
	.release	= mycdev_release,
	.read		= mycdev_read,
	.write		= mycdev_write,
	.llseek		= mycdev_llseek,
};
	
static int mycdev_init(void)
{
	int ret, devno;
	dev_t dev = MKDEV(mycdev_major, 0);
	//printk("null_init here.\n");
	if (mycdev_major)
		ret = register_chrdev_region(dev, 1, "mycdev");
	else {
		ret = alloc_chrdev_region(&dev, 0, 1, "mycdev");
		mycdev_major = MAJOR(dev);
	}
	if (ret < 0) {
		printk("mycdev: unable to get major %d.\n", mycdev_major);
		return ret;
	}
	
	devno = MKDEV(mycdev_major, 0);
	cdev_init(&mycDev, &fops);
	mycDev.owner = THIS_MODULE;
	mycDev.ops = &fops;
	ret = cdev_add(&mycDev, devno, 1);
	if (ret) {
		printk("mycdev: error %d adding mycdev.\n", ret);
	}
	
	mem_devp = kmalloc(sizeof(struct mem_dev), GFP_KERNEL);
	if (!mem_devp) {
		ret = -ENOMEM;
		goto fail_malloc;
	}
	memset(mem_devp, 0, sizeof(struct mem_dev));
	/*make error 
	mem_devp.size = MYCDEV_SIZE;
	mem_devp.data = kmalloc(MYCDEV_SIZE, GFP_KERNEL);
	memset(mem_devp.data, 0, MYCDEV_SIZE);	
	*/
	mem_devp[0].size = MYCDEV_SIZE;
	mem_devp[0].data = kmalloc(MYCDEV_SIZE, GFP_KERNEL);
	memset(mem_devp[0].data, 0, MYCDEV_SIZE);
	
	return 0;
	
	fail_malloc:
	unregister_chrdev_region(devno, 1);
	return ret;
}

static void mycdev_exit(void)
{
	//printk("null_exit here.\n");
	cdev_del(&mycDev);
	kfree(mem_devp);
	unregister_chrdev_region(MKDEV(mycdev_major, 0), 1);
}

module_init(mycdev_init);
module_exit(mycdev_exit);
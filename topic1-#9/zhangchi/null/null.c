#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <asm/uaccess.h>

#define P_DEBUG(fmt, args...) printk("<1>" "<kernel>[%s]"fmt, __FUNCTION__, ##args)


//===============================================================
int myopen(struct inode *node, struct file *filp)
{
	printk("test: null open\n");
	return 0;
}
int myclose(struct inode *node, struct file *filp)
{
	printk("test: null close\n");
	return 0;
}
//===============================

static ssize_t mn_read(struct file *filp, char __user *buf, size_t count, loff_t *offset)
{

	return count;
}

ssize_t mn_write(struct file *filp, char __user *buf, size_t count, loff_t *offset)
{
	return count;
}

//======================================================================


dev_t devno_n;
struct cdev mynull_cdev;


struct file_operations mynull_fops=
{
	.write = mn_write,
	.read = mn_read,
	.open = myopen,
	.release = myclose,
	//.owner = THIS_MODULE
};

static int __init my_init(void)
{
	int res;
	res = alloc_chrdev_region(&devno_n, 0, 1, "mynull");
	if(res < 0){
		P_DEBUG("register devno_n errno!\n");
		return res;
	}
	printk("devno_n allocated\n");
	cdev_init(&mynull_cdev, &mynull_fops);
	mynull_cdev.owner = THIS_MODULE;
	res = cdev_add(&mynull_cdev, devno_n, 1);
	if(res < 0){
		P_DEBUG("cdev_add errno!\n");
		unregister_chrdev_region(devno_n, 1);
		return res;
	}

	return 0;


}
static void __exit my_exit(void)
{
 	cdev_del(&mynull_cdev);
	unregister_chrdev_region(devno_n, 1);
	printk("test: done\n");

	return;
}

module_init(my_init);
module_exit(my_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Chi Zhang");
MODULE_VERSION("1.0");


#include "mydev.h"
#define DEVICE_NUM 0
static int dev_num = 0;
static int openNum = 0;

static int mydev_null_open(struct inode *inode, struct file *filp);
static int mydev_null_release(struct inode *inode, struct file* filp);
static ssize_t mydev_null_read(struct file *file, char __user *buf, size_t count, loff_t *f_pos);
static ssize_t mydev_null_write(struct file *file, const char __user *buf, size_t count, loff_t *f_pos);

static const struct file_operations mydev_file_operations = {
	.owner = THIS_MODULE,
	.read = mydev_null_read,
	.write = mydev_null_write,
	.open = mydev_null_open,
	.release = mydev_null_release, 
};

static int mydev_null_open(struct inode *inode, struct file *filp)
{	

	printk("\nMajor device number is %d, and the minor device number is %d\n", MAJOR(inode->i_rdev), MINOR(inode->i_rdev));
	if (openNum == 0) {
		++ openNum;
		return 0;
	}else {
		printk(KERN_ALERT "Another process open the char device.\n");
		return -1;
	}
}

static ssize_t mydev_null_read(struct file *file, char __user *buf, size_t count, loff_t *f_pos)
{
	return 0;
}

static ssize_t mydev_null_write(struct file *file, const char __user *buf, size_t count, loff_t *f_pos)
{
	printk("write success\n");
	return count;
}

static int mydev_null_release(struct inode *inode, struct file* filp)
{
	-- openNum;
	printk("The device is released!\n");
	return 0;
}

int mydev_null_init(void)
{
	int temp;
	printk(KERN_ALERT "Begin to init Char Device!\n");
	temp = register_chrdev(DEVICE_NUM, "mydev_null", &mydev_file_operations);
	if (temp < 0) {
		printk(KERN_WARNING "mydev_null: register failure\n");
		return -1;
	}else {
		printk("mydev_null: register success!\n");
		dev_num = temp;
		return 0;
	}
}

void mydev_null_exit(void)
{
	printk(KERN_ALERT "Unloading...\n");
	unregister_chrdev(dev_num, "mydev_null");
	printk("unregister success!\n");
}


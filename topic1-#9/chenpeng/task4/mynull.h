#ifndef MYNULL_H
#define MYNULL_H

#include <linux/fs.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/cdev.h>

#define MYNULL_MAJOR 1
#define NULL_DEV_NAME "mynull"

int __init mynull_init(void);
void __exit mynull_exit(void);
#endif
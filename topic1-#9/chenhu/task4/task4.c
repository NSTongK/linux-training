#include<linux/module.h>
#include<linux/init.h>
#include<linux/kernel.h>
#include<linux/fs.h>
#include<linux/types.h>
#include<asm/uaccess.h>
#include <linux/errno.h>
#include <linux/mm.h>
#include <linux/sched.h>
#include <linux/cdev.h>
#include <linux/slab.h>

#include "task4.h"

MODULE_LICENSE ("GPL");

int task4_init(void){
	printk("task4: init\n");
	return memory_init() || null_init () || zero_init ();
}

task4_cleanup(void){
	memory_cleanup();
	null_cleanup();
	zero_cleanup();
}

module_init (task4_init);
module_exit (task4_cleanup);


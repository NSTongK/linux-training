#include<linux/module.h>
#include<linux/init.h>
#include<linux/kernel.h>
#include<linux/fs.h>
#include<linux/types.h>
#include<asm/uaccess.h>
#include <linux/errno.h>
#include <linux/mm.h>
#include <linux/sched.h>
#include <linux/cdev.h>
#include <linux/slab.h>

MODULE_LICENSE ("GPL");

int memory_major = 101;
#define CDEV_NAME "memorydev"
#define LEN_BUF 0x1000

typedef struct memory_cdev{
	struct cdev cdev;
	char buffer[LEN_BUF];
}memory_cdev;
static struct memory_cdev *dev;



/*init_buf(void)
{
memset(buffer,0,LEN_BUF);

}*/
//open
static int
memory_open(struct inode *inode,struct file *filp)
{printk("memory driver open\n");
return 0;
}
//release
static int
memory_release(struct inode *inode,struct file *filp)
{printk("memory driver released\n");
return 0;
}
//write
static ssize_t
memory_write (struct file *filp, const char __user * buf, size_t count,
	     loff_t *f_pos)
{ssize_t err=-ENOMEM;
if(*f_pos>=LEN_BUF)
 goto ERR;
if(count>(LEN_BUF- *f_pos))
  count=LEN_BUF-*f_pos;
if(copy_from_user(dev->buffer+ *f_pos,buf,count))
 { err=-EFAULT;
 goto ERR;
 }
*f_pos+=count;
return count;
ERR:
  return err;
}
//read
static ssize_t
memory_read (struct file * filp, const char __user * buf, size_t count,
	    loff_t *f_pos)
{if(*f_pos>=LEN_BUF)
  return 0; 
 if(count>(LEN_BUF-*f_pos))
  count=LEN_BUF-*f_pos;
if(copy_to_user(buf,(void *)(dev->buffer+ *f_pos),count))
 return -EFAULT;
*f_pos+=count;
return count;
}

//lseek
static loff_t
memory_llseek(struct file *filp,loff_t off,int whence)
{loff_t pos;
pos=filp->f_pos;
switch(whence){
	case 0:
            {
	     pos=off;
                 break;
            }
	case 1:
          {pos+=off;
           break;
            }
	case 2:
      default:
             return -EINVAL;
          }
 if(pos>LEN_BUF||pos<0)
            return -EINVAL;
  filp->f_pos=pos;
  return filp->f_pos;	
}

struct file_operations memory_fops = {
.open=memory_open,
.release=memory_release,  
.read = memory_read,
  .write = memory_write,
  .llseek=memory_llseek,
  .owner = THIS_MODULE,
};

/*int
memory_init (void)
{
  
  if ((memory_major = register_chrdev (0, "memorydev", &memory_fops)) >= 0)
    { 
      printk ("register success!\n");
      return 1;
    }
  else
    {
      printk ("register failed!\n");
      return 0;
    }
}

memory_cleanup (void)
{
  unregister_chrdev (memory_major, "memorydev");
}

module_init (memory_init);
module_exit (memory_cleanup);*/
static void
 memory_setup_cdev(struct memory_cdev *dev, int index)
{
	int err, devno = MKDEV(memory_major, index);
	cdev_init(&dev->cdev, &memory_fops);
	dev->cdev.owner = THIS_MODULE;
	dev->cdev.ops = &memory_fops;
	err = cdev_add(&dev->cdev, devno, 1);
	if(err)
		printk("error %d adding memorydev %d\n", err, index);
}

static int
 memory_init(void)
{
	int result;
	dev_t devno = MKDEV(memory_major, 0);
	if(memory_major){
		result = register_chrdev_region(devno, 1, CDEV_NAME);
	}
	else{
		result = alloc_chrdev_region(&devno, 0, 1, CDEV_NAME);
		memory_major = MAJOR(devno);
	}
	if(result < 0){
		printk("Can't get major devno:%d \n",memory_major);
		return result;
	}
	dev = (memory_cdev *)kmalloc(sizeof(struct memory_cdev), GFP_KERNEL);
	if(!dev){
		result = -ENOMEM;
		goto failure;
	}
	memset(dev, 0, sizeof(struct memory_cdev));
	memory_setup_cdev(dev, 0);
	return 0;
failure:
	unregister_chrdev_region(devno, 1);
	return result;
}

static
 memory_cleanup(void)
{
	cdev_del(&dev->cdev);
	kfree(dev);
	unregister_chrdev_region(MKDEV(memory_major, 0), 1);
	
}

module_init(memory_init);
module_exit(memory_cleanup);




//blk_dev
//ppn = sector / 8; 
//pos = ppn * 4096 + offset; 
//file_write(buffer, nbytes, ppn, 0); 
//file_read(buffer, nbytes, ppn, 0); 
//这里，lpn与ppn一一对应，且支持就地更新。

第一种写测试：
1.  dd if=/dev/urandom of=/dev/my_bdeva bs=512 count=9
	9个sector的写引发了两个写请求，每次请求写一页，此时文件：
    File: ‘/tmp/blkdev_file’
    Size: 8192            Blocks: 16         IO Block: 4096   regular file
    Device: 804h/2052d      Inode: 147366085   Links: 1

	8192表示文件大小2个页（page 0 和page 1），16表示占了16个块（每块512B）。
	
2.  紧接着
	dd if=/dev/urandom of=/dev/my_bdeva bs=512 count=1
	此时文件：
    File: ‘/tmp/blkdev_file’
    Size: 8192            Blocks: 16         IO Block: 4096   regular file
    Device: 804h/2052d      Inode: 147366085   Links: 1

	看dmesg，发现是在page 0覆盖写。
	
3.   dd if=/dev/urandom of=/dev/my_bdeva bs=512 count=4 seek=16
	文件变为三页：
	File: ‘/tmp/blkdev_file’
	Size: 12288           Blocks: 128        IO Block: 4096   regular file
	Device: 804h/2052d      Inode: 147366085   Links: 1
	
	page_num 为2，seek值为16~23之间时，均为page 2。
	
=============================================================
1.  dd if=/dev/urandom of=/dev/my_bdeva bs=512 count=9
=============================================================
[  537.903661] begin of open
[  537.903669] begin of media_changed
[  537.903671] end of media_changed
[  537.903672] end of open
[  537.903932] mode 2: begin of make_request
[  537.903934] mode 2 in a make_requset:xfer_bio some sectors
[  537.903936] mode 1: begin of xfer_bio
[  537.903938] mode 1 in a xfer_bio:bdev_transfer some sectors
[  537.903939] begin of bdev_transfer
[  537.903951] file_read:read position: page_num: 0                 /////////////////////////////////
[  537.903953] file_read:read file: 4096
[  537.903955] bdev_transfer:read file: 4096
[  537.903957] end of bdev_transfer
[  537.903958] mode 1:end of xfer_bio
[  537.903961] mode 2: end of make_request
[  537.904746] mode 2: begin of make_request
[  537.904748] mode 2 in a make_requset:xfer_bio some sectors
[  537.904750] mode 1: begin of xfer_bio
[  537.904751] mode 1 in a xfer_bio:bdev_transfer some sectors
[  537.904753] begin of bdev_transfer
[  537.904797] file_read:read position: page_num: 1                ///////////////////////////////////
[  537.904800] file_read:read file: 4096
[  537.904802] bdev_transfer:read file: 4096
[  537.904820] end of bdev_transfer
[  537.904822] mode 1:end of xfer_bio
[  537.904824] mode 2: end of make_request
[  537.904859] mode 2: begin of make_request
[  537.904861] mode 2 in a make_requset:xfer_bio some sectors
[  537.904863] mode 1: begin of xfer_bio
[  537.904864] mode 1 in a xfer_bio:bdev_transfer some sectors
[  537.904866] begin of bdev_transfer
[  537.904918] file_write:write position: page_num: 0
[  537.904920] file_write:write file: 4096
[  537.904923] bdev_transfer:write file: 4096
[  537.904924] end of bdev_transfer
[  537.904925] mode 1:end of xfer_bio
[  537.904929] mode 2: end of make_request
[  537.904933] mode 2: begin of make_request
[  537.904935] mode 2 in a make_requset:xfer_bio some sectors
[  537.904936] mode 1: begin of xfer_bio
[  537.904938] mode 1 in a xfer_bio:bdev_transfer some sectors
[  537.904939] begin of bdev_transfer
[  537.905067] file_write:write position: page_num: 1
[  537.905070] file_write:write file: 4096
[  537.905072] bdev_transfer:write file: 4096
[  537.905074] end of bdev_transfer
[  537.905075] mode 1:end of xfer_bio
[  537.905078] mode 2: end of make_request
[  537.905101] begin of release
[  537.905103] end of release
[  537.905105] ===================end of release=============================


第二种测试：
1.  删除/tmp/blkdev_file，当前目录新建out文件
    echo "abcd" > out
	
2.  dd if=out of=/dev/my_bdeva bs=1 count=1
	文件的page 0内容为 a
	
	dd if=out of=/dev/my_bdeva bs=1 count=1 skip=1 seek=4096
	文件的page 1内容为 b
	
	dd if=out of=/dev/my_bdeva bs=1 count=1 skip=2 seek=8192
	文件的page 2内容为 c
	
	dd if=out of=/dev/my_bdeva bs=1 count=1 skip=3 seek=12288
	文件的page 3内容为 d











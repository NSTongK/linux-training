#B408 Linux培训

华中科技大学 信息存储与数字媒体实验室 B408 Linux培训。

#注意

[.gitignore](https://bitbucket.org/janzhou/linux-training/src/master/.gitignore)中增加了\*.out，以后所有编译生成的可执行文件都以out做后缀。

#工具手册

1. [Git使用方法](https://bitbucket.org/janzhou/linux-training/src/master/doc/git/git_usage.md)
2. [Markdown语法(英文)](https://bitbucket.org/janzhou/linux-training/src/master/doc/markdown.md)

#培训内容

1. [Topic1: #9 字符设备驱动](https://bitbucket.org/janzhou/linux-training/issue/9)
2. [Topic2: #3 块设备驱动](https://bitbucket.org/janzhou/linux-training/issue/3)
3. [Topic3: #2 内核和用户态共享内存](https://bitbucket.org/janzhou/linux-training/issue/2)
4. [Topic4: #5 内核与用户态通过Netlink通信](https://bitbucket.org/janzhou/linux-training/issue/5)
5. [Topic5: #6 内核中读写文件](https://bitbucket.org/janzhou/linux-training/issue/6)
